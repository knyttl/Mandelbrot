module Main where
 
import Graphics.HGL
import Debug.Trace
import Numeric
import Data.Word
 
-- mandelbrot bounds
bl, br, bt, bb :: Int
bl = -2               -- left  bounds  <-2,1)
br = 1                -- right bounds  (-2,1]
bb = -1               -- bottom bounds [-1,1)
bt = 1                -- top bounds    (-1,1]
 
-- width of the window
w = 460 :: Int
 
-- iteration count
it = 200 :: Int
 
-- definition of infinity
inf = 100 :: Double
 
-- complex plane sizes
sizex = abs $ bl - br
sizey = abs $ bb - bt 
 
-- height of the window
h = floor $ mi w (di sizey sizex)
 
-- steps for each pixel
stepx = di sizex w
stepy = di sizey h
 
-- int division & multiplication
di a b = (fromIntegral a)/(fromIntegral b)
mi :: Int -> Double -> Double
mi a b = (fromIntegral a)*b
 
-- test for belonging to mandelbrot set
mandel :: (Double,Double) -> Int
mandel c@(x,y) = mandel' it (0,0)
  where 
    mandel' it' zn@(a,b)
       | it' == 0        = 0
       | abs' zn < inf^2 = mandel' (pred it') (a*a-b*b+x,2*a*b+y)
       | otherwise       = it'
       where abs' (a,b)  = a*a+b*b
 
-- computation for each pixel
pixel :: Point -> Graphic
pixel (a,b) = withRGB rgb pixel'
  where 
    rgb | rg == 0 = (RGB 0 0 0) | otherwise = (RGB rg rg 255)
    rg = (floor . fromIntegral . highlight . mandel) pos
    pos = (mi a stepx + fromIntegral bl,mi b stepy + fromIntegral bb) -- position in complex
    pixel' = line (a,b) (a+1,b) -- HGL Bug: line of 1 pix won't be drawn in colour
    highlight i | i < 185 = i | otherwise = ((i-184)*20)  -- highlighting the differences
 
main :: IO ()
main = runGraphics $
           do win <- openWindow "Mandelbrot Set" (w,h)
              let screen = [(a,b) | a <- [0..w-1], b <- [0..h-1]]
              drawInWindow win $ overGraphics $ map pixel screen
              getKey win
              closeWindow win
